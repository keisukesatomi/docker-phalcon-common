FROM keisukesatomi/nginx-php-common

MAINTAINER corginia keisuke satomi <mlh38048@yahoo.co.jp>

WORKDIR /root

RUN apt-get update && apt-get install -y git-core gcc autoconf

RUN git clone --depth=1 git://github.com/phalcon/cphalcon.git

WORKDIR cphalcon/build/safe

RUN export CFLAGS="-O2 -fvisibility=hidden" && phpize && ./configure --enable-phalcon && make && make install

RUN echo -e "; priority=30 \nextension=phalcon.so"  > /etc/php5/mods-available/phalcon.ini

RUN php5enmod phalcon

WORKDIR /root

RUN git clone git://github.com/phalcon/phalcon-devtools.git

WORKDIR phalcon-devtools

RUN ./phalcon.sh

RUN ln -s /root/phalcon-devtools/phalcon.php /usr/bin/phalcon

RUN chmod ugo+x /usr/bin/phalcon
